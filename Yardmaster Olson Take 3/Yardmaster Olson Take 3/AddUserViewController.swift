//
//  AddUserViewController.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/30/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation
import FirebaseDatabase
import UIKit

class AddUserViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var errorL: UILabel!
    @IBOutlet weak var doneB: UIBarButtonItem!
    
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var datesMowedTF: UITextField!
    
    @IBOutlet weak var moneyP: UIPickerView!
    
    var price: Int = 0
    
    @IBAction func doneAdding(_ sender: Any) {
        let database = Database.database().reference().child("ScottsUsers")
        
        self.nameTF.text = self.nameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            
        database.queryOrdered(byChild: "Name").queryEqual(toValue: nameTF.text).observe(.value, with: { snapshot in
            
            if snapshot.exists() || self.nameTF.text!.count == 0 {
                self.errorL.text = "Already in list"
                self.errorL.isHidden = false
                print("Already in list")
            } else {
                self.price = self.moneyP.selectedRow(inComponent: 0)*100 + self.moneyP.selectedRow(inComponent: 1)*10 +
                    self.moneyP.selectedRow(inComponent: 2)*5
                
                //To fix no address/dates mowed bug
                if self.addressTF.text!.isEmpty {
                    self.addressTF.text = "Default"
                }
                if self.datesMowedTF.text!.isEmpty {
                    self.datesMowedTF.text = "Default"
                }
                if self.phoneTF.text!.isEmpty {
                    self.phoneTF.text = "123-456-7890"
                }
                
                let user : NSDictionary = ["Name" : self.nameTF.text!, "Phone" : self.phoneTF.text!, "Address" : self.addressTF.text!, "DatesMowed" : self.datesMowedTF.text!, "Price" : self.price]
                database.child(self.nameTF.text!).setValue(user) {
                    (error, ref) in
                    if error != nil {
                        print(error!)
                    } else {
                        print("Success!!!")
                    }
                }
                self.performSegue(withIdentifier: "scottsPage", sender: Any.self)
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressTF.delegate = self
        phoneTF.delegate = self
        nameTF.delegate = self
        datesMowedTF.delegate = self
        
        moneyP.delegate = self
        moneyP.dataSource = self
        
        nameTF.becomeFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 2 {
            return 2
        } else {
            return 10
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Insides setting titles")
        if component == 2 && row == 1 {
            return String(5)
        }
        return String(row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)

        return false
    }
}
