//
//  PreviewViewController.swift
//  Print2PDF
//
//  Created by Gabriel Theodoropoulos on 14/06/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit
import MessageUI
import WebKit

class PreviewViewController: UIViewController {

    //@IBOutlet weak var webPreview: UIWebView!
    @IBOutlet weak var webPreview: WKWebView!
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    var invoiceInfo: [String: AnyObject]! = [:]/*= ["invoiceNumber" : "Default" as AnyObject,
                                             "invoiceDate" : "Some String" as AnyObject,
                                             "recipientInfo" : "Some String" as AnyObject,
                                             "items" : [["item": "mowed", "price": "10"]] as AnyObject,
                                             "totalAmount" : "Total Amount" as AnyObject]
    */
    var invoiceComposer: InvoiceComposer!
    
    var HTMLContent: String!
    
    var nextNumberAsString: String!
    var invoiceNumber: String!
    var currentUser = User()
    
    var specialItems = [[String:String]]()
    
    @IBOutlet weak var addSpecialB: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        determineInvoiceNumber()
        
        invoiceInfo["invoiceNumber"] = invoiceNumber as AnyObject?
        navigationBar.title = "Invoice " + invoiceNumber
        
        createInvoiceAsHTML()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let page = segue.destination as! DetailPageViewController
        let information = invoiceInfo["recipientInfo"] as! String
        var lines = information.split { $0.isNewline }
        let items = invoiceInfo["items"] as! [[String: String]]
        print(lines)
        print(invoiceInfo!)
        print("items", items as Any)
        print(items[0]["item"] as Any, "item object")
        
        page.fillData(name: String(lines[0]), phone: currentUser.phone, address: String(lines[1]), dates: currentUser.dates, price: currentUser.price)
    }

    // MARK: IBAction Methods
    
    @IBAction func exportToPDF(_ sender: AnyObject) {
        invoiceComposer.exportHTMLContentToPDF(HTMLContent: HTMLContent)
        showOptionsAlert()
        if nextNumberAsString != nil {
            print("Actually setting next invoice number")
            UserDefaults.standard.set(nextNumberAsString, forKey: "nextInvoiceNumber")
        }
        else {
            print("Setting default")
            UserDefaults.standard.set("000002", forKey: "nextInvoiceNumber")
        }
    }
    
    @IBAction func addSpecialItem(_ sender: Any) {
        var itemTextField = UITextField()
        var priceTextField = UITextField()
        
        itemTextField.delegate = self as? UITextFieldDelegate
        priceTextField.delegate = self as? UITextFieldDelegate

        
        let alert = UIAlertController(title: "Add a Special Item", message: "Add your special item and price here. Then double check it is correct.", preferredStyle: UIAlertController.Style.alert)
        let someAction = UIAlertAction(title: "Add Item", style: UIAlertAction.Style.default, handler: {(action) -> Void in
            if itemTextField.text!.count > 0, let _ = Int(priceTextField.text!) {
                self.specialItems.append(["specialItem" : itemTextField.text!, "price":priceTextField.text!])
                print("Come on", self.specialItems)
                print("Debugging")
                self.createInvoiceAsHTML()
            } else {
                print("I guess that works")
            }
        })
        alert.addAction(someAction)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Item w/ Description"
            itemTextField = textField
        })
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField?.delegate = self as? UITextFieldDelegate
            textField?.placeholder = "Price"
            priceTextField = textField
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Custom Methods
    
    func fillInvoiceData(user: User) {
        let datesArray = user.dates.split(separator: " ")
        print("datesArray", datesArray)
        invoiceInfo["invoiceNumber"] = 000000 as AnyObject
        invoiceInfo["invoiceDate"] = self.formatAndGetCurrentDate() as AnyObject
        invoiceInfo["recipientInfo"] = "\(user.name)\n\(user.address)\n" as AnyObject
        
        var items = [NSDictionary]()
        var total = 0
        
        for i in 0..<datesArray.count {
            items.append(["item" : String(datesArray[i]), "price" : "\(user.price)"])
            total = total + user.price
        }
        
        print(items)
        invoiceInfo["items"] = items as AnyObject
        invoiceInfo["totalAmount"] = "\(total)" as AnyObject
        currentUser = user
    }
    
    func formatAndGetCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        return dateFormatter.string(from: NSDate() as Date)
    }
    
    func determineInvoiceNumber() {
        // Get the invoice number from the user defaults if exists.
        
        //print(UserDefaults.standard.object(forKey: "nextInvoiceNumber")!)
        if let nextInvoiceNumber = UserDefaults.standard.object(forKey: "nextInvoiceNumber") {
            
            invoiceNumber = (nextInvoiceNumber as? String)!
            
            // Save the next invoice number to the user defaults.
            let nextNumber = Int(nextInvoiceNumber as! String)! + 1
            
            if nextNumber < 10 {
                nextNumberAsString = "00000\(nextNumber)"
            }
            else if nextNumber < 100 {
                nextNumberAsString = "0000\(nextNumber)"
            }
            else if nextNumber < 1000 {
                nextNumberAsString = "000\(nextNumber)"
            }
            else if nextNumber < 10000 {
                nextNumberAsString = "00\(nextNumber)"
            }
            else if nextNumber < 100000 {
                nextNumberAsString = "0\(nextNumber)"
            }
            else {
                nextNumberAsString = "\(nextNumber)"
            }
        }
        else {
            // Specify the first invoice number.
            invoiceNumber = "000001"
        }
        
        // Set the invoice number to the navigation bar's title.
    }
    
    func createInvoiceAsHTML() {
        invoiceComposer = InvoiceComposer()
        
        if let invoiceHTML = invoiceComposer.renderInvoice(invoiceNumber: invoiceInfo["invoiceNumber"] as! String,
                                                           invoiceDate: invoiceInfo["invoiceDate"] as! String,
                                                           recipientInfo: invoiceInfo["recipientInfo"] as! String,
                                                           items: invoiceInfo["items"] as! [[String: String]],
                                                           totalAmount: invoiceInfo["totalAmount"] as! String, specialItems: self.specialItems) {
            //print(invoiceComposer.pathToInvoiceHTMLTemplate!)
            webPreview.loadHTMLString(invoiceHTML, baseURL: NSURL(string: invoiceComposer.pathToInvoiceHTMLTemplate!) as URL?)
            HTMLContent = invoiceHTML
        }
        print("HTML Content ---- ", HTMLContent!)
    }
    
    
    
    func showOptionsAlert() {
        let alertController = UIAlertController(title: "Yeah!", message: "Your invoice has been successfully saved to a PDF file.\n\nGo to your files to do more with the invoice.", preferredStyle: UIAlertController.Style.alert)
        
        let actionNothing = UIAlertAction(title: "Go Back", style: UIAlertAction.Style.default) { (action) in
            
        }
        
        alertController.addAction(actionNothing)
        
        present(alertController, animated: true, completion: nil)
    }
}
