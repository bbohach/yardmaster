//
//  DetailPageViewController.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/31/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class DetailPageViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var priceP: UIPickerView!
    @IBOutlet weak var messageP: UIPickerView!
    
    var invoiceInfo: [String: AnyObject]! = [:]
    
    var presetMessages: [String] = []
    var pickerRow: Int = -1
    var originalDates: String = ""
    var user = User()

    //var datePicker: UIPickerView! = UIPickerView()
    //var dateToAdd: String = ""
    
    let personalMessagesDB = Database.database().reference().child("PersonalMessages")
    let datesMowedDB = Database.database().reference().child("ScottsUsers")
    
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var phoneB: UIButton!
    @IBOutlet weak var phraseToAddTF: UITextField!
    @IBOutlet weak var editPageB: UIBarButtonItem!
    @IBOutlet weak var addPhraseB: UIButton!
    @IBOutlet weak var deletePhraseB: UIButton!
    @IBOutlet weak var customMesageTF: UITextView!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var invoiceB: UIButton!
    @IBOutlet weak var datesField: UITextField!
    
    @IBAction func callingUser(_ sender: Any) {
        if let url = URL(string: "tel://\(user.phone)") {
            UIApplication.shared.open(url)
        }
    }
    @IBAction func deletingPhrase(_ sender: Any) {
        if presetMessages.count > 1  && presetMessages[pickerRow] != "Other" {
            let removePhrase = personalMessagesDB.child(String(pickerRow))
            removePhrase.removeValue()
            presetMessages.remove(at: pickerRow)
            messageP.reloadAllComponents()
        }
    }
    
    @IBAction func editingThePage(_ sender: Any) {
        if addPhraseB.isHidden {
            addPhraseB.isHidden = false
            deletePhraseB.isHidden = false
            phraseToAddTF.isHidden = false
            phoneB.isHidden = true
            phoneField.text = user.phone
            phoneField.isHidden = false
            addressField.isEnabled = true
            editPageB.title = "Done"
            priceP.isUserInteractionEnabled = true
            invoiceB.isEnabled = false
            datesField.isEnabled = true
            
        } else {
            addPhraseB.isHidden = true
            deletePhraseB.isHidden = true
            phraseToAddTF.isHidden = true
            addressField.isEnabled = false
            phoneB.isHidden = false
            phoneField.isHidden = true
            editPageB.title = "Edit"
            priceP.isUserInteractionEnabled = false
            invoiceB.isEnabled = true
            datesField.isEnabled = false
            
            for x in [addressField, phoneField, phraseToAddTF] {
                if textFieldShouldReturn(x!) {
                    print("Success")
                }
            }
            
            
            user.address = addressField.text!
            user.phone = phoneField.text!
            user.dates = datesField.text!
            
            phoneB.titleLabel?.text = user.phone
            
            user.price = self.priceP.selectedRow(inComponent: 0)*100 + self.priceP.selectedRow(inComponent: 1)*10 +
                self.priceP.selectedRow(inComponent: 2)*5
            
            datesMowedDB.child(user.name).child("DatesMowed").setValue(self.user.dates)
            datesMowedDB.child(user.name).child("Address").setValue(user.address)
            datesMowedDB.child(user.name).child("Phone").setValue(user.phone)
            datesMowedDB.child(user.name).child("Price").setValue(user.price)
        }
    }
    
    @IBAction func addingPhrase(_ sender: Any) {
        if phraseToAddTF.text!.count > 0 {
            personalMessagesDB.queryEqual(toValue: phraseToAddTF.text).observe(.value, with: {snapshot in
                if snapshot.exists() {
                    print("Oops")
                } else {
                    self.presetMessages.append(self.phraseToAddTF.text!)
                    print(self.presetMessages)
                    //let message : NSDictionary = ["Message\(self.presetMessages.count)" : self.phraseToAddTF.text!]
                    self.personalMessagesDB.setValue(self.presetMessages)
                    self.messageP.reloadAllComponents()
                    self.phraseToAddTF.text = ""
                    if self.textFieldShouldReturn(self.phraseToAddTF) {
                        print("im here")
                    } else {
                        print("Im not there")
                    }
                }
            }) 
        } else {
            phraseToAddTF.isHighlighted = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        priceP.delegate = self
        priceP.dataSource = self
        messageP.delegate = self
        messageP.dataSource = self
        
        phoneField.delegate = self
        addressField.delegate = self
        
        customMesageTF.delegate = self
        
        phraseToAddTF.delegate = self

        
        messageP.layer.cornerRadius = 50
        
        var tempPrice = user.price % 100
        priceP.selectRow(user.price / 100, inComponent: 0, animated: true)
        
        priceP.selectRow(tempPrice / 10, inComponent: 1, animated: true)
        
        tempPrice = tempPrice % 10
        priceP.selectRow(tempPrice / 5, inComponent: 2, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        print(user.name)
        navigation.title = "\(user.name)'s Page"
        addressField.text = user.address
        datesField.text = user.dates
        
        phoneB.setTitle(user.phone, for: UIControl.State.normal)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*if originalDates != clientDatesServiced {
            datesMowedDB.queryOrdered(byChild: "Name").queryEqual(toValue: clientName).observe(.value, with: {snapshot in
                if snapshot.exists() {
                    print("In add mow", self.clientDatesServiced)
                    print(snapshot, snapshot.childrenCount)
                    let snapshotValue = snapshot.value as! NSDictionary
                    let user = snapshotValue.value(forKey: self.clientName) as! NSDictionary
                    print(user.value(forKey: "DatesMowed")!)
                    user.setValue("\(self.clientDatesServiced)", forKey: "DatesMowed")
                    print(user)
                    print(snapshot.value!)
                    print(self.datesMowedDB.child("\(self.clientName)"))
                    self.datesMowedDB.child(self.clientName).removeValue()
                    self.datesMowedDB.child(self.clientName).setValue(user)
                     
                } else {
                    print("Oops")
                }
            })
        }*/
        
        if let identifier = segue.identifier {
            if identifier == "previewPage" {
                let previewViewController = segue.destination as! PreviewViewController
                previewViewController.fillInvoiceData(user: user)
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if phoneField.isEditing || addressField.isEditing {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if phoneField.isEditing || addressField.isEditing {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Choose Other, customize your message here!"
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Choose Other, customize your message here!" {
            textView.text = ""
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == messageP {
            return 1
        } else {
            return 3
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == messageP {
            pickerRow = messageP.selectedRow(inComponent: 0)
            return presetMessages.count
        } else {
            if component == 2 {
                return 2
            } else {
                return 10
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Insides setting titles")
        if pickerView == messageP {
            return presetMessages[row]
        } else {
            if component == 2 && row == 1 {
                return String(5)
            }
            return String(row)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == messageP {
            pickerRow = row
        } /*else if pickerView == datePicker {
            var month = 0
            var day = 0
            if component == 0 {
                month = row
            } else {
                day = row
            }
            dateToAdd = "\(month)/\(day)"
        }*/
    }
    
    func fillData(name: String, phone: String, address: String, dates: String, price: Int) {
        user.name = name
        user.phone = phone
        user.address = address
        user.dates = dates
        originalDates = dates
        user.price = price
        
        personalMessagesDB.observe(.childAdded, with: {snapshot in
            print("This is it", snapshot, snapshot.value ?? "")
            if snapshot.value as? String != "" && !self.presetMessages.contains(snapshot.value as! String) {
                self.presetMessages.append(snapshot.value as! String)
            }
            self.messageP.reloadAllComponents()
        })
    }
}
