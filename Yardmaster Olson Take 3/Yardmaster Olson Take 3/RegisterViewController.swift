//
//  ViewController.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/22/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var pinTF: UITextField!
    @IBOutlet weak var errorL: UILabel!
    @IBOutlet weak var signInB: UIButton!
    
    @IBOutlet weak var YardmasterL: UILabel!
    
    var info: NSDictionary = [:]
    
    @IBAction func signingIn(_ sender: Any) {
        if phoneTF.text == "5672244279" {
            self.performSegue(withIdentifier: "scottsPage", sender: self)
        } else {
            self.performSegue(withIdentifier: "homePage", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.phoneTF.delegate = self
        self.pinTF.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        YardmasterL.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is HomePageViewController {
            
            let homePageFiller = segue.destination as! HomePageViewController
            homePageFiller.fillData(data: info)
        }
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        self.performSegue(withIdentifier: "scottsPage", sender: self)

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        if phoneTF.text?.count != 0 && pinTF.text?.count != 0 {
            errorL.isHidden = false
        }
        if validPhone() && validPin() {
            errorL.text = "Valid credentials"
            isAUser()
        } else if validPhone() {
            errorL.text = "PIN not valid"
            self.signInB.isEnabled = false
        } else if validPin() {
            errorL.text = "Phone # not valid"
            self.signInB.isEnabled = false
        } else {
            errorL.text = "Both field not valid"
            self.signInB.isEnabled = false
        }
        return false
    }
    
    func validPhone() -> Bool {
        if Int(phoneTF.text!) ?? 0 != 0 && phoneTF.text?.count == 10 {
            return true
        }
        return false
    }
    
    func validPin() -> Bool {
        let intText = Int(pinTF.text!) ?? 0
        if intText != 0 && pinTF.text?.count == 4 {
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if textField == phoneTF {
            return newLength <= 10
        } else {
            return newLength <= 4
        }
    }
    
    func isAUser() {
        let messageDB = Database.database().reference().child("Users")
        
        messageDB.queryOrdered(byChild: "Phone").queryEqual(toValue: phoneTF.text)
            .observe(.value, with: { snapshot in
                print(snapshot.value!)
                
                if snapshot.exists() {
                    
                    let snapshotValue = snapshot.value as! NSDictionary
                    print(snapshotValue.value(forKey: "5672241760") as Any)
                    let tempInfo = snapshotValue.value(forKey: self.phoneTF.text!) as! NSDictionary
                    
                    if tempInfo.value(forKey: "PIN") as? String == self.pinTF.text {
                        self.info = tempInfo
                        print("found it")
                        self.signInB.isEnabled = true
                    } else {
                        self.signInB.isEnabled = false
                        self.errorL.isHidden = false
                        self.errorL.text = "Incorrect PIN"
                    }
                } else {
                    self.signInB.isEnabled = false
                    print("no email found")
                }
            })
        
        //messageDB.observe(.childAdded, with: { snapshot in
        //
        //    let snapshotValue = snapshot.value as! NSDictionary
        //    print(snapshotValue)
        //    let pin = snapshotValue["PIN"] as! String
        //    let phoneNumber = snapshotValue["Phone"] as! String
        //    print(snapshotValue["PIN"]!, pin)
        //    print(snapshotValue["Phone"]!, phoneNumber)
        //
        //    if phoneNumber == self.phoneTF.text {
        //        if pin == self.pinTF.text {
        //        completion()
        //        }
        //    }
        //    print(phoneNumber == self.phoneTF.text)
        //    print(pin == self.pinTF.text)
        //})
    }
}


