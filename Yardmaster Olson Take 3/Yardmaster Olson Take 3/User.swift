//
//  User.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 7/4/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation

class User {
    var name: String = ""
    var phone: String = ""
    var address: String = ""
    var dates: String = ""
    var price: Int = -1
    var specialDescriptions: String = ""
}
