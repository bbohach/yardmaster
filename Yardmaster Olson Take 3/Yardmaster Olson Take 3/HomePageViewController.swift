//
//  HomePageViewController.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/22/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import FirebaseDatabase

class HomePageViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var signOutB: UIBarButtonItem!
    @IBOutlet var navigation: UINavigationItem!
    @IBOutlet weak var scottsMessage: UITextView!
    
    var info: NSDictionary = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let database = Database.database().reference().child("Message")
        
        print("Im here")
        database.observe(.childAdded, with: { snapshot in
            print("Snapshot value", snapshot.value! )
            if snapshot.exists() {
                self.scottsMessage.text = snapshot.value as? String
            } else {
                self.scottsMessage.text = "This doesn't work"
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Info", info)
        navigation.title = "Welcome \(info.value(forKey: "Name") as? String ?? "shithead")!"
        
        // set initial location in Honolulu
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        
        centerMapOnLocation(location: initialLocation)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func fillData(data: NSDictionary) {
        info = data
        print(data)
    }
}
