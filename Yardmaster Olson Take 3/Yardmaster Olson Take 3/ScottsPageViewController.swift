//
//  ScottsPage.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/28/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class ScottsPageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    var nameList: [String] = []
    var phoneList: [String] = []
    var addressList: [String] = []
    var datesMowedList: [String] = []
    var priceList: [Int] = []
    var originalMessage: String = ""
    var didChange: Bool = false
    var firstLoad: Bool = true
    var deletedUsers: [String] = []
    
    let scottsUsersDatabase = Database.database().reference().child("ScottsUsers")
    
    var selectedName: Int = -1
    
    @IBOutlet weak var userList: UITableView!
    @IBOutlet weak var scottsMessage: UITextView!
    @IBOutlet weak var messageStatus: UILabel!
    @IBOutlet weak var editB: UIBarButtonItem!
    @IBOutlet weak var addUserB: UIButton!
    
    @IBAction func editPressed(_ sender: Any) {
        if self.addUserB.isHidden == false {
            //self.userList.isEditing = false
            self.addUserB.isHidden = true
            self.editB.title = "Edit"
            //let database = Database.database().reference().child("ScottsUsers")
            print("Edit just pressed", nameList, nameList.count)
            if didChange {
                fillTableView()
            }
        } else {
            didChange = false
            //self.userList.isEditing = true
            self.addUserB.isHidden = false
            self.editB.title = "Done"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillData(completion: {
            print("reload data")
            self.userList.reloadData()
        })
        self.userList.delegate = self
        self.userList.dataSource = self
        self.scottsMessage.delegate = self
        
        self.userList.layer.cornerRadius = 20
        let database = Database.database().reference().child("Message")
        
        database.observe(.childAdded, with: { snapshot in
            if snapshot.exists() {
                self.scottsMessage.text = snapshot.value as? String
                self.originalMessage = self.scottsMessage.text
            } else {
                self.scottsMessage.text = "This doesn't work"
            }
        })
        //if firstLoad {
        //    let defaults = UserDefaults.standard
        //    print(defaults.array(forKey: "names") as Any)
        //    nameList = defaults.array(forKey: "names") as! [String]
        //    phoneList = defaults.array(forKey: "phones") as! [String]
        //    addressList = defaults.array(forKey: "addresses") as! [String]
        //    print("In firstLoad", nameList)
        //    firstLoad = false
        //} else {
        //    print("Should be here", nameList)
        //}
        userList.reloadData()
    }
    
    override func viewDidLoad() {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsPage" {
        
            let page = segue.destination as! DetailPageViewController
            page.fillData(name: nameList[selectedName], phone: phoneList[selectedName], address: addressList[selectedName], dates: datesMowedList[selectedName], price: priceList[selectedName])
        }
    }
    
    func fillTableView() {
        scottsUsersDatabase.removeValue()
        if nameList.count >= 1 {
            for i in 0...nameList.count-1 {
                let user : NSDictionary = ["Name" : self.nameList[i], "Phone" : self.phoneList[i], "Address" : self.addressList[i], "DatesMowed" : self.datesMowedList[i], "Price" : self.priceList[i]]
                scottsUsersDatabase.child(self.nameList[i]).setValue(user)
                
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.messageStatus.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let database = Database.database().reference().child("Message")
        
        if textView.text! != originalMessage {
            database.updateChildValues(["Message" : textView.text!]) {
                (error, ref) in
                if error != nil {
                    print(error!)
                    self.messageStatus.text = "Error, send help"
                }
                else {
                    print("Message saved successfully!")
                    self.messageStatus.text = "Message Sent!"
                    self.messageStatus.isHidden = false
                    self.originalMessage = textView.text!
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            scottsUsersDatabase.child(nameList[indexPath.row]).removeValue()
            //self.deletedUsers.append(nameList[indexPath.row])
            self.nameList = self.nameList.filter{$0 != nameList[indexPath.row]}
            self.phoneList = self.phoneList.filter{$0 != phoneList[indexPath.row]}
            self.addressList = self.addressList.filter{$0 != addressList[indexPath.row]}
            self.datesMowedList = self.datesMowedList.filter{$0 != datesMowedList[indexPath.row]}
            self.priceList = self.priceList.filter{$0 != priceList[indexPath.row]}
            
            print("Inside delete function", self.nameList)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        
            //fillTableView()
            
            didChange = true
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let nameObject = self.nameList[sourceIndexPath.row]
        let phoneObject = self.phoneList[sourceIndexPath.row]
        let addressObject = self.addressList[sourceIndexPath.row]
        let datesMowedObject = self.datesMowedList[sourceIndexPath.row]
        let priceObject = self.priceList[sourceIndexPath.row]
        
        nameList.remove(at: sourceIndexPath.row)
        phoneList.remove(at: sourceIndexPath.row)
        addressList.remove(at: sourceIndexPath.row)
        datesMowedList.remove(at: sourceIndexPath.row)
        priceList.remove(at: sourceIndexPath.row)

        nameList.insert(nameObject, at: destinationIndexPath.row)
        phoneList.insert(phoneObject, at: destinationIndexPath.row)
        addressList.insert(addressObject, at: destinationIndexPath.row)
        datesMowedList.insert(datesMowedObject, at: destinationIndexPath.row)
        priceList.insert(priceObject, at: destinationIndexPath.row)

        didChange = true
        print("Insid3 moving function", nameList)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Lawns"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return nameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)
        cell.textLabel?.text = "\(nameList[indexPath.row]) - $\(priceList[indexPath.row])"
        print(indexPath.row, addressList, datesMowedList)
        cell.detailTextLabel?.text = "\(addressList[indexPath.row]) | \(datesMowedList[indexPath.row])"
        cell.layer.cornerRadius = 10
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(nameList[indexPath.row])
        selectedName = indexPath.row
        performSegue(withIdentifier: "detailsPage", sender: Any?.self)
    }
    
    
    
    func fillData(completion: @escaping () -> ()) {
            
        print("Inside fill list", nameList)
        
        scottsUsersDatabase.observe(.childAdded, with: { snapshot in
            print(snapshot, snapshot.value!, "Something")
                let snapshotValue = snapshot.value as! NSDictionary
                
                if self.nameList.contains(snapshotValue.value(forKey: "Name") as! String) || self.deletedUsers.contains(snapshotValue.value(forKey: "Name") as! String) {
                    print("Already in list or got deleted")
                } else {
                    self.nameList.append(snapshotValue.value(forKey: "Name") as! String)
                    self.phoneList.append(snapshotValue.value(forKey: "Phone") as! String)
                    self.addressList.append(snapshotValue.value(forKey: "Address") as! String)
                    self.datesMowedList.append(snapshotValue.value(forKey: "DatesMowed") as! String)
                    self.priceList.append(snapshotValue.value(forKey: "Price") as! Int)
                    print("Inside fill list", snapshotValue.value(forKey: "Name") as! String)
                }
                completion()
        })
    }
}

