//
//  InvoiceComposer.swift
//  Print2PDF
//
//  Created by Gabriel Theodoropoulos on 23/06/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

class InvoiceComposer: NSObject {

    let pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "invoice", ofType: "html")
    
    let pathToSingleItemHTMLTemplate = Bundle.main.path(forResource: "single_item", ofType: "html")
    
    let pathToLastItemHTMLTemplate = Bundle.main.path(forResource: "last_item", ofType: "html")
    
    let senderInfo = "Scott Olson<br>805 Armstrong Dr.<br>567-224-4279"
    
    let dueDate = "Due Date: 30 days"
    
    let paymentMethod = "Cash | Check | Venmo | PayPal"
    
    let logoImageURL = "Olson The Yardmaster"
    
    var invoiceNumber: String!
    
    var pdfFilename: String!
    
    override init() {
        super.init()
    }
    
    
    func renderInvoice(invoiceNumber: String, invoiceDate: String, recipientInfo: String, items: [[String: String]], totalAmount: String, specialItems: [[String:String]]) -> String! {
        // Store the invoice number for future use.
        self.invoiceNumber = invoiceNumber
        
        var newTotal = Int(totalAmount)
        
        do {
            
            // Load the invoice HTML template code into a String variable.
            var HTMLContent = try String(contentsOfFile: pathToInvoiceHTMLTemplate!)
            
            // Replace all the placeholders with real values except for the items.
            // The logo image.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#LOGO_IMAGE#", with: logoImageURL)
            
            // Invoice number.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#INVOICE_NUMBER#", with: invoiceNumber)
            
            // Invoice date.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#INVOICE_DATE#", with: invoiceDate)
            
            // Due date (we leave it blank by default).
            HTMLContent = HTMLContent.replacingOccurrences(of: "#DUE_DATE#", with: dueDate)
            
            // Sender info.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#SENDER_INFO#", with: senderInfo)
            
            // Recipient info.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#RECIPIENT_INFO#", with: recipientInfo.replacingOccurrences(of: "\n", with: "<br>"))
            
            // Payment method.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#PAYMENT_METHOD#", with: paymentMethod)
            
            // The invoice items will be added by using a loop.
            var allItems = ""
            
            // For all the items except for the last one we'll use the "single_item.html" template.
            // For the last one we'll use the "last_item.html" template.
            for i in 0..<items.count {
                var itemHTMLContent: String!
                
                // Determine the proper template file.
                    if i != items.count - 1 || specialItems.count != 0 {
                        itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                    }
                    else {
                        itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)
                    }
                    
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i]["item"]! + " - Mowed")
                    
                    let formattedPrice = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(value: items[i]["price"]!)
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: formattedPrice)
                allItems += itemHTMLContent

                    print(specialItems.count, specialItems)
            }
                for x in 0..<specialItems.count {
                    var itemHTMLContent: String!
                    if x != specialItems.count - 1 {
                        itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                    } else {
                        itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)

                    }
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: specialItems[x]["specialItem"]!)
                        
                    let formattedPrice = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(value: specialItems[x]["price"]!)
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: formattedPrice)
                        
                    allItems += itemHTMLContent
                        
                    let something = specialItems[x]["price"]!
                    newTotal = newTotal! + Int(something)!
                }
                
                
                // Replace the description and price placeholders with the actual values.
                /*if (items[i]["item"]?.contains("*"))! {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i]["item"]!)
                } else {
                    itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i]["item"]! + " - Mowed")
                }*/
                
                // Format each item's price as a currency value.
                /*let formattedPrice = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(value: items[i]["price"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: formattedPrice)*/
                
                // Add the item's HTML code to the general items string.
            
            // Total amount.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#TOTAL_AMOUNT#", with: String(newTotal!))
            
            // Set the items.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ITEMS#", with: allItems)
            
            // The HTML code is ready.
            return HTMLContent
            
        }
        catch {
            print("Unable to open and use HTML template files.")
        }
        
        return nil
    }
    
    
    func exportHTMLContentToPDF(HTMLContent: String) {
        let printPageRenderer = CustomPrintPageRenderer()
        
        let printFormatter = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer: printPageRenderer)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let manager = FileManager.default
        
        
        if manager.isWritableFile(atPath: documentsPath) {
            pdfFilename = "\(documentsPath)/Invoice\(invoiceNumber!).pdf"
            pdfData?.write(toFile: pdfFilename, atomically: true)
            print("ah ha, GOT IT!!!")
        } else {
            print("Well shiiiiiiiiiiit super")
        }
        print(AppDelegate.getAppDelegate().getDocDir())
        print(pdfFilename ?? "Some Error...shit")
    }
    
    func drawPDFUsingPrintPageRenderer(printPageRenderer: UIPrintPageRenderer) -> NSData! {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, CGRect.zero, nil)
        for i in 0..<printPageRenderer.numberOfPages {
            UIGraphicsBeginPDFPage()
            printPageRenderer.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
        }
        
        UIGraphicsEndPDFContext()
        
        print("Data", data)
        
        return data
    }
    
}
