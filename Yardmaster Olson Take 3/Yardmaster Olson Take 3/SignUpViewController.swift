//
//  SignUp.swift
//  Yardmaster Olson Take 3
//
//  Created by brandon bohach on 5/22/19.
//  Copyright © 2019 Olson The Yardmaster. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseAuth

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var feedbackTF: UITextField!
    @IBOutlet weak var pinTF: UITextField!
    @IBOutlet weak var confirmPinTF: UITextField!
    @IBOutlet weak var verificationCodeTF: UITextField!
    
    @IBOutlet weak var verifyPhoneB: UIButton!
    @IBOutlet weak var signUpB: UIButton!
    
    @IBOutlet weak var pinErrorL: UILabel!
    @IBOutlet weak var confirmPinErrorL: UITextView!
    @IBOutlet weak var phoneErrorL: UILabel!
    @IBOutlet weak var pinQualificationsTV: UITextView!
    
    var globalVerificationID: String!
    var valid: Bool = false
    
    @IBAction func outsideTouched(_ sender: Any) {
        
        if textFieldShouldReturn(sender as! UITextField) {
            print("yup")
        }
    }
    
    
    @IBAction func signUpPressed(_ sender: UIButton) {
        let messagesDB = Database.database().reference().child("Users")
        
        let messageDictionary : NSDictionary = ["Name" : nameTF.text!, "Phone" : phoneTF.text!, "Address" : addressTF.text!, "Feedback" : feedbackTF.text!, "PIN" : pinTF.text!]
        messagesDB.child(phoneTF.text!).setValue(messageDictionary) {
            (error, ref) in
            if error != nil {
                print(error!)
            }
            else {
                print("Message saved successfully!")
            }
        }
    }
    
    @IBAction func verifyPhonePressed(_ sender: Any) {
        PhoneAuthProvider.provider().verifyPhoneNumber("+1\(phoneTF.text!)", uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                self.confirmPinErrorL.isHidden = false
                self.confirmPinErrorL.text = error.localizedDescription
                return
                }
            
            self.globalVerificationID = verificationID
            
            print(verificationID as Any)
            
            self.verificationCodeTF.isEnabled = true
            // Sign in using the verificationID and the code sent to the user
                // ...
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinQualificationsTV.layer.cornerRadius = 10

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        self.nameTF.delegate = self
        self.addressTF.delegate = self
        self.phoneTF.delegate = self
        self.feedbackTF.delegate = self
        self.pinTF.delegate = self
        self.confirmPinTF.delegate = self
        self.verificationCodeTF.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        switch textField {
        case pinTF:
            if validPin() {
                pinErrorL.text = "Valid PIN"
            }
        case confirmPinTF:
            if validConfirmPin() {
                print("validConfirmPin")
            }
        case verificationCodeTF:
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: globalVerificationID, verificationCode: verificationCodeTF.text!)
            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                if let error = error {
                    self.confirmPinErrorL.isHidden = false
                    self.confirmPinErrorL.text = error as? String
                    self.signUpB.isEnabled = false
                    print("Verification Error", error)
                    return
                }
                self.signUpB.isEnabled = true

            }
        default:
            print("Doing nothing")
        }
        
        if validPhone() {
            isAUser(completion: {
                if self.valid {
                    self.confirmPinErrorL.isHidden = false
                    self.confirmPinErrorL.text = "Phone # Already Registered"
                    self.verifyPhoneB.isEnabled = false
                } else {
                    if self.comparePins() {
                        self.verifyPhoneB.isEnabled = true
                    } else {
                        self.verifyPhoneB.isEnabled = false
                        self.confirmPinErrorL.isHidden = false
                    }
                }
            })
        } else {
            verifyPhoneB.isEnabled = false
        }
        
        
        return false
    }
    
    func validPhone() -> Bool {
        if phoneTF.text?.count != 0 {
            if Int(phoneTF.text!) ?? 0 == 0 {
                phoneErrorL.isHidden = false
            } else {
                if phoneTF.text?.count == 10 {
                    phoneErrorL.isHidden = true
                    return true
                }
            }
        }
        return false
    }
    
    func validPin() -> Bool {
        let intText = Int(pinTF.text!) ?? 0
        pinErrorL.isHidden = false
        if intText == 0 || pinTF.text?.count != 4 {
            pinErrorL.text = "4 numbers only!"
        } else {
            return true
        }
        return false
    }
    
    func validConfirmPin() -> Bool {
        let intText = Int(confirmPinTF.text!) ?? 0
        if intText == 0 || confirmPinTF.text?.count != 4 {
            confirmPinErrorL.isHidden = false
            confirmPinErrorL.text = "4 numbers only!"
        } else {
            return true
        }
        return false
    }
    func comparePins() -> Bool {
        if pinTF.text?.count == 4 && Int(pinTF.text!) ?? 0 != 0 {
            if pinTF.text == confirmPinTF.text {
                confirmPinErrorL.isHidden = false
                confirmPinErrorL.text = "You're all set, sign up!"
                pinErrorL.isHidden = true
                return true
            } else {
                confirmPinErrorL.text = "PIN's don't match"
            }
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        switch textField {
        case phoneTF:
            return newLength <= 10
        case pinTF, confirmPinTF:
            return newLength <= 4
        default:
            return newLength <= 30
            
            
        }
    }
    func isAUser(completion: @escaping () -> ()) {
        let messageDB = Database.database().reference().child("Users")
        
        messageDB.queryOrdered(byChild: "Phone").queryEqual(toValue: phoneTF.text)
            .observe(.value, with: { snapshot in
                print(snapshot.value!)
                print(snapshot.exists())
                if snapshot.exists() {
                    self.valid = true
                } else {
                    self.valid = false
                    print("no email found")
                }
                completion()
            })
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        
        for x in [nameTF, addressTF, phoneTF, feedbackTF, pinTF, confirmPinTF, verificationCodeTF] {
            if (x == phoneTF || x == pinTF || x == confirmPinTF || x == verificationCodeTF) &&                                                                               x!.isEditing {
                if textFieldShouldReturn(x!) {
                    print("Yup")
                }
            }
            x?.resignFirstResponder()
        }
    }
}
